/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pankaz.liferayshoppingcart.tests;

import com.pankaz.liferayshoppingcart.dto.Product;
import com.pankaz.liferayshoppingcart.taxlogic.TaxCalc;
import java.io.FileNotFoundException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class TestTaxCalc {
    TaxCalc taxCalc;
    Product product1;
    Product product2;
    Product product3;
    
    public TestTaxCalc() {       
        
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws FileNotFoundException {
        product1 = new Product();
        product1.setProductName("bottle of perfume");
        product1.setUnitPrice(27.99);
        product1.setQuantity(1);
        product1.setImported(true);
        product1.setFoodMedicineBooks(false);

        product2 = new Product();
        product2.setProductName("bottle of perfume");
        product2.setUnitPrice(18.99);
        product2.setQuantity(1);
        product2.setImported(false);
        product2.setFoodMedicineBooks(false);
        
        taxCalc = new TaxCalc(product1);
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void testGetImportedTax(){
        //Arrange
        double knownValue = 0.05;
        //Act
        double importedTax = taxCalc.getImportTax();
        //Assert
        assertEquals(importedTax, knownValue,0.001);
    }
    @Test
    public void testGetSalesTax(){
        //Arrange
        double knownValue = 0.1;
        //Act
        double salesTax = taxCalc.getSalesTax();
        //Assert
        assertEquals(salesTax, knownValue,0.001);
    }
    
    @Test 
    public void testRounDown(){
        //Arrange
        double num = 0.123456;
        double expectedValue = 0.12;
        //Act
        double value = taxCalc.roundDown2(num);
        //Assert
        assertEquals(expectedValue, value,0.001);
    }
    
    @Test
    public void testRoundOff(){
        //Arrange
        double num = 0.5625;
        double expectedValue = 0.6;
        //Act
        double value = taxCalc.roundOff(num);
        //assert
        assertEquals(expectedValue, value,0.0001);
    }
    
    @Test
    public void testCalculateImportTax(){
        //Arrange
        double knownImportTax = 1.4;
        //act
        double importTax = taxCalc.calculateImportTax();
        //assert
        assertEquals(importTax, knownImportTax,0.0001);
        
    }
    
    @Test
    public void testCalculateSalesTax(){
        //Arrange
        double knownSalesTax = 2.8;
        //act
        double salesTax = taxCalc.calculateSalesTax();
        //assert
        assertEquals(salesTax, knownSalesTax,0.0001);
    }
    
    @Test
    public void testCalculateTotalTax(){
         //Arrange
         double importTax = taxCalc.calculateImportTax();
         double salesTax = taxCalc.calculateSalesTax();
        double knownTotalTax = importTax+salesTax;
        //act
        double totalTax = taxCalc.calculateTax();
        //assert
        assertEquals(totalTax, knownTotalTax,0.0001);
        
    }
}
