/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pankaz.liferayshoppingcart.tests;

import com.pankaz.liferayshoppingcart.dao.TaxDaoImpl;
import com.pankaz.liferayshoppingcart.dto.TaxDto;
import java.io.FileNotFoundException;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class TestTaxDaoImpl {
    TaxDaoImpl dao;
    TaxDto salesTax;
    TaxDto importTax;
    public TestTaxDaoImpl() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        dao = new TaxDaoImpl(); 
        
        salesTax = new TaxDto();
        salesTax.setTaxType("SalesTax");
        salesTax.setTaxRate(10.0);
        
        importTax = new TaxDto();
        importTax.setTaxType("ImportTax");
        importTax.setTaxRate(5.0);
        
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    @Test
    public void testAllTaxInfo() throws FileNotFoundException{
        //Arrange
        dao.loadTax();
        //Act
        List<TaxDto> listTaxDto = dao.getAllTaxInfo();
        TaxDto taxDtoObj1 = listTaxDto.get(0);
        TaxDto taxDtoObj2 = listTaxDto.get(1);
        
        //Assert
        assertEquals(salesTax, taxDtoObj1);
        assertEquals(importTax, taxDtoObj2);
        
    }
}
