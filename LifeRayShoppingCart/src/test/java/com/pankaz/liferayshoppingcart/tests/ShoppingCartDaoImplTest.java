/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pankaz.liferayshoppingcart.tests;

import com.pankaz.liferayshoppingcart.dao.ShoppingCartDao;
import com.pankaz.liferayshoppingcart.dao.ShoppingCartDaoImpl;
import com.pankaz.liferayshoppingcart.dto.Product;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Lenovo
 */
public class ShoppingCartDaoImplTest {

    ShoppingCartDao dao;
    Product product1;
    Product product2;
    Product product3;

    public ShoppingCartDaoImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        dao = new ShoppingCartDaoImpl();

        product1 = new Product();
        product1.setProductName("bottle of perfume");
        product1.setUnitPrice(27.99);
        product1.setQuantity(1);
        product1.setImported(true);
        product1.setFoodMedicineBooks(false);

        product2 = new Product();
        product2.setProductName("bottle of perfume");
        product2.setUnitPrice(18.99);
        product2.setQuantity(1);
        product2.setImported(false);
        product2.setFoodMedicineBooks(false);
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAddProduct() {
        //Arrange

        //Act
        Product prod = dao.addProduct(product1);
        //Assert
        assertNotEquals(prod.getProductId(), 0);
    }

    @Test
    public void testGetProductById() {
        //Arrange
        Product prod = dao.addProduct(product1);
        //Act
        Product prodById = dao.getProductById(prod.getProductId());
        //Assert
        assertEquals(prod, prodById);
    }

    @Test
    public void testGetProducts() {
        //Arrange
        Product prod1 = dao.addProduct(product1);
        Product prod2 = dao.addProduct(product2);
        //Act
        List<Product> productList = dao.getProducts();
        //Assert
        assertNotNull(productList);
        assertEquals(productList.size(), 2);

    }

    @Test
    public void testDeleteProductById() {
        //Arrange
        Product prod1 = dao.addProduct(product1);
        //Act
        dao.deleteProductById(prod1.getProductId());
        Product prodById = dao.getProductById(prod1.getProductId());
        //Assert
        assertNull(prodById);
    }
    
    @Test
    public void testEmptyShoppingCart(){
        //Arrange
        Product prod1 = dao.addProduct(product1);
        Product prod2 = dao.addProduct(product2);
        //Act
        dao.emptyShoppingCart();
        Product prodById = dao.getProductById(prod1.getProductId());
        List<Product> productList = dao.getProducts();       
        
        //Assert
        assertNull(prodById);
        assertTrue(productList.isEmpty());
        
    }
}
