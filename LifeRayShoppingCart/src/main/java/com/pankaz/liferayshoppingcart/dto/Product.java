/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pankaz.liferayshoppingcart.dto;

/**
 *
 * @author Lenovo
 */
public class Product {

    private int productId;
    private String productName;
    private boolean foodMedicineBooks;
    private boolean imported;
    private double unitPrice;
    private int quantity;
    private double totalTax;
    private double totalPrice;
    private String descriptionWithoutTax;
    private String descriptionWithTax;

    public String getDescriptionWithTax() {
        return descriptionWithTax;
    }

    public void setDescriptionWithTax(String descriptionAfterTax) {
        this.descriptionWithTax = descriptionAfterTax;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public boolean isFoodMedicineBooks() {
        return foodMedicineBooks;
    }

    public void setFoodMedicineBooks(boolean foodMedicineBooks) {
        this.foodMedicineBooks = foodMedicineBooks;
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(double totalTax) {
        this.totalTax = totalTax;
    }

    public String getDescriptionWithoutTax() {
        return descriptionWithoutTax;
    }

    public void setDescriptionWithoutTax(String description) {
        this.descriptionWithoutTax = description;
    }

}
