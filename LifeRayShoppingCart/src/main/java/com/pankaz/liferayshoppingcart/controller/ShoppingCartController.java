/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pankaz.liferayshoppingcart.controller;

import com.pankaz.liferayshoppingcart.taxlogic.TaxCalc;
import com.pankaz.liferayshoppingcart.dao.ShoppingCartDao;
import com.pankaz.liferayshoppingcart.dao.ShoppingCartDaoImpl;
import com.pankaz.liferayshoppingcart.dto.Product;
import com.pankaz.liferayshoppingcart.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class ShoppingCartController {

    private ConsoleIO con = new ConsoleIO();

    private ShoppingCartDao dao = new ShoppingCartDaoImpl();
    boolean loop = true;

    public void run() {

        while (loop) {

            try {
                printMenu();
                int userSelection = con.readInt("\nPlease select option from above menu", 1, 5);
                switch (userSelection) {
                    case 1:
                        addProduct();
                        break;
                    case 2:
                        removeProduct();
                        break;
                    case 3:
                        printReceipt();
                        break;
                    case 4:

                        loadExampleCartsValidate();
                        break;
                    case 5:
                        con.print("Quitting");
                        loop = false;
                        break;
                }
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ShoppingCartController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void printMenu() {
        con.print("*****************************************************");
        con.print("Welcome to the LifeRay shopping cart "
                + "\nEnterprise. Open Source. For Life. \n");
        displayShoppingCartProducts();
        con.print("\nPlease select from the options below");

        con.printSameLn("1) Add to shopping cart"
                + "\n2) Remove item from shopping cart"
                + "\n3) Print receipt"
                + "\n4) Print receipt for example carts"
                + "\n5) Quit");

    }
    
    /**
     * ask user for information to create product object, calculate all tax and total for the object and 
     * add the object to the database
     * @throws FileNotFoundException 
     */
    private void addProduct() throws FileNotFoundException {
        //getting input from the user
        String productRaw = con.readString("Please Enter the product name: ");
        String productName = Character.toUpperCase(productRaw.charAt(0)) + productRaw.substring(1);

        int quantity = con.readInt("Please enter the quantity: ");

        //userInput for foodMedBook
        boolean cond1 = true;
        boolean foodMedBooksBool = false;
        while (cond1) {
            String foodMedBooks = con.readString("Books Medicines and food are exempt from sales tax."
                    + "\nIs this one of these items. Yes/No: ");

            if (foodMedBooks.equalsIgnoreCase("YES") || foodMedBooks.equalsIgnoreCase("Y")) {
                foodMedBooksBool = true;
                cond1 = false;
            } else if (foodMedBooks.equalsIgnoreCase("NO") || foodMedBooks.equalsIgnoreCase("N")) {
                foodMedBooksBool = false;
                cond1 = false;
            } else {
                cond1 = true;
            }
        }
        //user input for imported
        boolean cond2 = true;
        boolean importBool = false;
        while (cond2) {
            String imported = con.readString("Is this an imported product. Yes/No: ");

            if (imported.equalsIgnoreCase("Yes") || imported.equalsIgnoreCase("Y")) {
                importBool = true;
                cond2 = false;
            } else if (imported.equalsIgnoreCase("NO") || imported.equalsIgnoreCase("N")) {
                importBool = false;
                cond2 = false;
            } else {
                cond2 = true;
            }
        }

        double unitPrice = con.readDouble("Please Enter the Unit price for " + productName + ":");

        //Creating new product object and setting all the properties of the product object
        Product product = new Product();
        product.setProductName(productName);
        product.setImported(importBool);
        product.setFoodMedicineBooks(foodMedBooksBool);
        product.setUnitPrice(unitPrice);
        product.setQuantity(quantity);

        //calculating tax on the product
        TaxCalc tax = new TaxCalc(product);
        double totalTax = tax.calculateTax();
        product.setTotalTax(totalTax);

        //Calculate total price
        double totalPrice = roundDown2((unitPrice * quantity) + totalTax);
        product.setTotalPrice(totalPrice);

        String importedStr = "";
        if (product.isImported()) {
            importedStr = "imported";
        }

        String productDescriptionWithoutTax = product.getQuantity() + " " + importedStr + " " + product.getProductName()
                + " at $" + product.getUnitPrice();
        product.setDescriptionWithoutTax(productDescriptionWithoutTax);

        String productDescriptionWithTax = product.getQuantity() + " " + importedStr + " " + product.getProductName()
                + " for $" + product.getTotalPrice();
        product.setDescriptionWithTax(productDescriptionWithTax);

        dao.addProduct(product);
        con.readString("Item added to the cart. Please press enter to continue ");

    }

    public double roundDown2(double d) {
        return Math.round(d * 1e2) / 1e2;
    }
    /**
     * displays all shopping cart products with price before tax
     */
    private void displayShoppingCartProducts() {
        List<Product> allProducts = dao.getProducts();
        if (allProducts.size() > 0) {
            con.print("\nYour shopping cart detail");
            for (Product p : allProducts) {
                con.print(p.getDescriptionWithoutTax());
            }
        }
    }
    /**
     * List all products and ask user to select product to delete
     */
    private void removeProduct() {
        List<Product> allProducts = dao.getProducts();
        if (allProducts.isEmpty()) {
            con.readString("There are no items in your shopping cart. Please press enter to continue ");
        } else {

            for (int i = 1; i <= allProducts.size(); i++) {
                con.print(i + ") " + allProducts.get(i - 1).getDescriptionWithoutTax());
            }
            int userSelection = con.readInt("Please select from the option to delete item", 1, allProducts.size());
            int deleteProductId = allProducts.get(userSelection - 1).getProductId();
            dao.deleteProductById(deleteProductId);
            con.readString("Item deleted. Please press enter to continue......");
        }
    }
    /**
     * Prints receipt for the cart. 
     */
    private void printReceipt() {
        List<Product> allProducts = dao.getProducts();
        if (allProducts.isEmpty()) {
            con.readString("There are no items in your shopping cart. "
                    + "\nPlease add items in the shopping cart to print receipt."
                    + "\nHit enter to continue");
        } else {

            con.print("\nReceipt for LifeRay shopping cart");
            con.print("**********************************");
            double total = 0;
            double totalSalesTax = 0;
            for (Product p : allProducts) {
                con.print(p.getDescriptionWithTax());
                total += p.getTotalPrice();
                totalSalesTax += p.getTotalTax();
            }
            con.printSameLn("Sales Taxes: $");
            System.out.printf("%.2f", totalSalesTax);
            con.printSameLn("\nTotal: $");
            System.out.printf("%.2f", total);
            con.print("\n**********************************");
            con.readString("Please hit enter to continue");
        }
    }

    public double roundOff(double amount) {
        amount = Math.ceil(amount * 20) / 20.0;
        return amount;

    }

    //Everything below is created to show example carts only. All of these data uses the methods from this controller
    //class and utilize the dao layer.
    /**
     * validate if any product is already defined by the user. If true ask user if they want to continue with the example
     * cart. Do so will override user defined cart with example cart.
     * @throws FileNotFoundException 
     */
    private void loadExampleCartsValidate() throws FileNotFoundException {
        if (dao.getProducts().size() > 0) {
            String userInput = con.readString("Seems like your cart has item in it."
                    + "\nIf you continue to example cart, your current cart will be deleted."
                    + "\nContinue.. Yes/No");
            if (userInput.equalsIgnoreCase("yes") || userInput.equalsIgnoreCase("y")) {
                loadExampleCarts();
            }

        } else {
            loadExampleCarts();
        }
    }
    /**
     * ask user to choose example cart to load it and print at the console 
     * @throws FileNotFoundException 
     */
    private void loadExampleCarts() throws FileNotFoundException {
        dao.emptyShoppingCart();
        menuForExampleCarts();
        int userInput = con.readInt("Select from the above example carts to print receipt", 1, 3);
        switch (userInput) {
            case 1:
                loadCart1();
                break;
            case 2:
                loadCart2();
                break;
            case 3:
                loadCart3();
                break;
        }

    }
    /**
     * Load example carts and print receipt for the cart. After everything it empty the database
     * @throws FileNotFoundException 
     */
    private void loadCart1() throws FileNotFoundException {
        Product book = createProductObjectForExampleCarts("book", true, false, 12.49, 1);
        Product musicCD = createProductObjectForExampleCarts("music CD", false, false, 14.99, 1);
        Product chocolateBar = createProductObjectForExampleCarts("chocolate bar", true, false, 0.85, 1);
        con.print("\nPrinting receipt for Cart1");
        printReceipt();
        dao.emptyShoppingCart();
    }

    private void loadCart2() throws FileNotFoundException {
        Product chocolates = createProductObjectForExampleCarts("box of chocolates", true, true, 10.00, 1);
        Product perfume = createProductObjectForExampleCarts("bottle of perfume", false, true, 47.50, 1);
        con.print("\nPrinting receipt for Cart2");
        printReceipt();
        dao.emptyShoppingCart();
    }

    private void loadCart3() throws FileNotFoundException {
        Product importedPerfume = createProductObjectForExampleCarts("bottle of perfume", false, true, 27.99, 1);
        Product perfume = createProductObjectForExampleCarts("bottle of perfume", false, false, 18.99, 1);
        Product pills = createProductObjectForExampleCarts("packet of headache pills", true, false, 9.75, 1);
        Product chocolateBar = createProductObjectForExampleCarts("box of chocolate bar", true, true, 11.25, 1);
        con.print("\nPrinting receipt for Cart3");
        printReceipt();
        dao.emptyShoppingCart();
    }
    /**
     * creates product object and adds it to the database. This method is to be used only for example cart
     * @param productName
     * @param foodMedBook
     * @param imported
     * @param unitPrice
     * @param quantity
     * @return
     * @throws FileNotFoundException 
     */
    private Product createProductObjectForExampleCarts(String productName, boolean foodMedBook, boolean imported, double unitPrice, int quantity) throws FileNotFoundException {
        Product product = new Product();
        product.setProductName(productName);
        product.setFoodMedicineBooks(foodMedBook);
        product.setImported(imported);
        product.setUnitPrice(unitPrice);
        product.setQuantity(quantity);
        TaxCalc tax = new TaxCalc(product);
        double totalTax = tax.calculateTax();
        product.setTotalTax(totalTax);

        //Calculate total price
        double totalPrice = roundDown2((unitPrice * quantity) + totalTax);
        product.setTotalPrice(totalPrice);

        String importedStr = "";
        if (product.isImported()) {
            importedStr = "imported";
        }

        String productDescriptionWithoutTax = product.getQuantity() + " " + importedStr + " " + product.getProductName()
                + " at $" + product.getUnitPrice();
        product.setDescriptionWithoutTax(productDescriptionWithoutTax);

        String productDescriptionWithTax = product.getQuantity() + " " + importedStr + " " + product.getProductName()
                + " for $" + product.getTotalPrice();
        product.setDescriptionWithTax(productDescriptionWithTax);

        dao.addProduct(product);
        return product;
    }
    
    /**
     * Prints input information of the example carts
     */
    private void menuForExampleCarts() {
        con.print("Displaying example carts.......\n");
        con.print("1) Cart 1"
                + "\n\t1 book at $12.49"
                + "\n\t1 music cd at $14.99"
                + "\n\t1 chocolate bar at $0.85");
        con.print("\n2) Cart 2"
                + "\n\t1 imported box of chocolates at $10.00"
                + "\n\t1 imported bottle of perfume at $47.50");
        con.print("\n3) Cart 3"
                + "\n\t1 imported bottle of perfume at $27.99"
                + "\n\t1 bottle of perfume at $18.99"
                + "\n\t1 packet of headache pills at $9.75"
                + "\n\t1 imported box or chocolates at $11.25");

    }
}
