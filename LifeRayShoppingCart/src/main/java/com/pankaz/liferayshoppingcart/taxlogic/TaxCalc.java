/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pankaz.liferayshoppingcart.taxlogic;

import com.pankaz.liferayshoppingcart.dao.TaxDaoImpl;
import com.pankaz.liferayshoppingcart.dto.Product;
import com.pankaz.liferayshoppingcart.dto.TaxDto;
import java.io.FileNotFoundException;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public class TaxCalc {

    TaxDaoImpl taxDao;
    private double unitPrice;
    private int quantity;
    private boolean foodMediceneBooks;
    private boolean imported;

    public TaxCalc(Product product) throws FileNotFoundException {
        this.unitPrice = product.getUnitPrice();
        this.quantity = product.getQuantity();
        this.foodMediceneBooks = product.isFoodMedicineBooks();
        this.imported = product.isImported();
        taxDao = new TaxDaoImpl();
        taxDao.loadTax();
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isFoodMediceneBooks() {
        return foodMediceneBooks;
    }

    public void setFoodMediceneBooks(boolean foodMediceneBooks) {
        this.foodMediceneBooks = foodMediceneBooks;
    }

    public boolean isImported() {
        return imported;
    }

    public void setImported(boolean imported) {
        this.imported = imported;
    }

    public double calculateTax() {
        double importTax = calculateImportTax();
        double salesTax = calculateSalesTax();
        double totalTax = roundOff(importTax+salesTax);

        return totalTax;
    }

    public double calculateSalesTax() {
        double salesTax = 0;
        if (foodMediceneBooks) {
            return salesTax;
        } else {
             salesTax = roundOff(getSalesTax() * unitPrice * quantity);
             return salesTax;
        }

    }

    public double calculateImportTax() {

        double importTax = 0;
        if (imported == false) {
            return importTax;
        } else {
             importTax = roundOff(getImportTax() * unitPrice * quantity);
             return importTax;
        }

    }

    public double roundOff(double amount) {
        amount = Math.ceil(amount * 20) / 20.0;
        return amount;

    }

    public double roundDown2(double d) {
        return Math.floor(d * 1e2) / 1e2;
    }

    public double getSalesTax() {
        List<TaxDto> listOfTax = taxDao.getAllTaxInfo();
        double salesTaxPercent = listOfTax.get(0).getTaxRate();
        double salesTax = salesTaxPercent / 100;
        return salesTax;
    }

    public double getImportTax() {
        List<TaxDto> listOfTax = taxDao.getAllTaxInfo();
        double salesTaxPercent = listOfTax.get(1).getTaxRate();
        double salesTax = salesTaxPercent / 100;
        return salesTax;
    }

}
