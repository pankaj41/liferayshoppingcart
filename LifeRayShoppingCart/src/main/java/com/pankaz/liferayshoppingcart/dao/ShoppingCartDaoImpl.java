/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pankaz.liferayshoppingcart.dao;

import com.pankaz.liferayshoppingcart.dto.Product;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Lenovo
 */
public class ShoppingCartDaoImpl implements ShoppingCartDao {

    //This is the runTime database used to store data
    private Map<Integer, Product> shoppingCartMap = new HashMap<>();
    private int counter = 1;

    @Override
    public Product addProduct(Product prod) {
        prod.setProductId(counter);
        shoppingCartMap.put(counter, prod);
        counter++;
        return prod;
    }

    @Override
    public List<Product> getProducts() {
        Collection<Product> c = shoppingCartMap.values();
        return new ArrayList(c);
    }

    @Override
    public void deleteProductById(int id) {
        shoppingCartMap.remove(id);
    }

    @Override
    public Product getProductById(int id) {
       return shoppingCartMap.get(id);
    }

    @Override
    public void emptyShoppingCart() {
        shoppingCartMap.clear();
    }

}
