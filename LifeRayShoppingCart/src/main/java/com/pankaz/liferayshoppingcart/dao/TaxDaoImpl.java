/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pankaz.liferayshoppingcart.dao;

import com.pankaz.liferayshoppingcart.dto.TaxDto;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class TaxDaoImpl {
    private static final String  DELIMITER = ",";
    private List<TaxDto> taxArrayList = new ArrayList<>();
    
    public void loadTax() throws FileNotFoundException{
        Scanner sc = new Scanner(new BufferedReader(new FileReader("TaxRate.txt")));
        String currentLine;
        String[] currentTokens;
        
        while (sc.hasNext()) {
            currentLine = sc.nextLine();
            currentTokens = currentLine.split(DELIMITER);
            
            TaxDto tax = new TaxDto();
            tax.setTaxType(currentTokens[0]);
            tax.setTaxRate(Double.parseDouble(currentTokens[1]));
            
            taxArrayList.add(tax);
        }
        sc.close();
    }
    
    public List<TaxDto> getAllTaxInfo(){
        return this.taxArrayList;
    }
    
}
