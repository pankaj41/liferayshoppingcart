/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pankaz.liferayshoppingcart.dao;

import com.pankaz.liferayshoppingcart.dto.Product;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface ShoppingCartDao {
    public Product addProduct(Product prod);
    public Product getProductById(int id);
    public List<Product> getProducts();
    public void deleteProductById(int id);
    public void emptyShoppingCart();
}
