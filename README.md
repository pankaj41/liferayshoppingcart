# README LifeRay Shopping Cart#

LifeRay Shopping Cart is console based java project to demonstrate Object Oriented Principle in programming. This project is created using n-tire design and uses run time data structure to store data. UML diagram for the project is included with the code.This project is 100% test covered using junit. Main purpose of the project is to add/remove items in/from shopping cart and print receipt. This program calculates the sales tax and import tax based on the product type and import status. Import tax and sales tax are imported from TaxRate.txt file.    

Project starts with main menu  
1) Add to shopping cart  
2) Remove item from shopping cart  
3) Print receipt  
4) Print receipt for example carts  
5) Quit  
  
Currently project can add and remove items from the cart and print receipt. 
I've included option "4) Print receipt from example carts" so that the example carts from the coding problem can be processed and receipt can be printed with the predefined object in the program. 


### How do I get set up? ###
**Netbeans**  
This project was created using netbeans IDE. Once cloned, project should be as simple as open project, clean built and run.   
**For Eclipse**  
Follow the steps to run the project in eclipse  
1) Open Create new Java Project window file> New> Java Project  
2) Name Project Name as "LifeRayShoppingCart".  
3) Browse to location of folder "LifeRayShoppingCart" from the cloned folder then hit finish.  
 This should open the project in eclipse. If there is any error with test do the following   
4) Navigate to src/test/java file in eclipse and create a new junit test file. This will import junit jar   to the project and all the test should work . The newly created test file can be deleted.   
 

### Contact me ###  
Feel free to contact me for questions and comments  
pankaj.nuaa@gmail.com